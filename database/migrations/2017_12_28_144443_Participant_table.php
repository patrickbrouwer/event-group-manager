<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParticipantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id',false,true);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('sex');
            $table->boolean('mentor')->default('0');
            $table->date('birthdate');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->string('phone',12);
            $table->string('phone_emergency',12);
            $table->string('department');
            $table->string('shirt_size');
            $table->text('allergies')->nullable();
            $table->text('diet')->nullable();
            $table->text('medical')->nullable();
            $table->text('comments')->nullable();
            $table->string('document_nr');
            $table->string('insurer');
            $table->string('insurer_number');
            $table->boolean('payed')->default('0');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
