<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupParticipantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group-particiapant', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id',false,true)->index();
            $table->integer('participant_id',false,true)->index();
            $table->timestamps();

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('participant_id')->references('id')->on('participants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group-particiapant');
    }
}
