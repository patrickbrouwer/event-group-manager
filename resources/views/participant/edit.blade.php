@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{route('participant.update',[$participant->id])}}" method="post">
            {!! method_field('patch') !!}
            @include('participant.form')

            <button type="submit" class="btn btn-success">Opslaan</button>
        </form>
    </div>
@endsection