@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1>Deelnemer detail</h1>

        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mr-2" role="group" aria-label="First group">
                <a href="{{route('participant.edit',$participant->id)}}" class="btn btn-info">Bewerken</a>
            </div>
            <div class="btn-group mr-2" role="group" aria-label="Second group">
                <a href="#" class="btn btn-danger">Verwijderen</a>
            </div>
        </div>

        <table class="table table-bordered">
            <tr>
                <th>Mentor</th>
                <th>Betaald</th>
            </tr>
            <tr>
                <td>@if($participant->mentor == 1) <span class="text-success">Ja</span> @else Nee @endif </td>
                <td>@if($participant->payed == 1) <span class="text-success">Ja</span> @else <span class="text-danger">Nee</span> @endif </td>
            </tr>
        </table>

        <table class="table table-striped table-sm">
            <tbody>

                <tr>
                    <th>ID</th>
                    <td>{{$participant->id}}</td>
                </tr>
                <tr>
                    <th>Geslacht</th>
                    <td>@if($participant->sex == 'M') Man @else Vrouw @endif</td>
                </tr>
                <tr>
                    <th>Voornaam</th>
                    <td>{{$participant->first_name}}</td>
                </tr>
                <tr>
                    <th>Achternaam</th>
                    <td>{{$participant->last_name}}</td>
                </tr>
                <tr>
                    <th>Geboortedatum</th>
                    <td>{{date('d-m-Y',strtotime($participant->birthdate))}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{$participant->email}}</td>
                </tr>
                <tr>
                    <th>Adres</th>
                    <td>{{$participant->address}}</td>
                </tr>
                <tr>
                    <th>Stad</th>
                    <td>{{$participant->city}}</td>
                </tr>
                <tr>
                    <th>Telefoon</th>
                    <td>{{$participant->phone}}</td>
                </tr>
                <tr>
                    <th>Telefoon calamiteiten</th>
                    <td>{{$participant->phone_emergency}}</td>
                </tr>
                <tr>
                    <th>Afdeling</th>
                    <td>{{$participant->department}}</td>
                </tr>
                <tr>
                    <th>Shirt maat</th>
                    <td>{{$participant->shirt_size}}</td>
                </tr>
                <tr>
                    <th>Overig</th>
                    <td>{{$participant->comments}}</td>
                </tr>
            </tbody>
        </table>

    </div>
@endsection