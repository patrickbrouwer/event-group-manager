@include('partials.formError')

{{ csrf_field() }}
<div class="form-group">
    <label>Voornaam </label>
    <input type="text" class="form-control" name="first_name" value="{{ old('first_name',$participant->first_name) }}" readonly>
</div>

<div class="form-group">
    <label>Voornaam </label>
    <input type="text" class="form-control" name="last_name" value="{{ old('last_name',$participant->last_name) }}" readonly>
</div>

<div class="form-group">
    <label for="payedSelector">Betaald</label>
    <select class="form-control" name="payed" id="payedSelector" required>
        <option value="0" @if(old('payed',$participant->payed) == 0) selected @endif>Nee</option>
        <option value="1" @if(old('payed',$participant->payed) == 1) selected @endif>Ja</option>
    </select>
</div>

<div class="form-group">
    <label for="mentorSelector">Mentor</label>
    <select class="form-control" name="mentor" id="mentorSelector" required>
        <option value="0" @if(old('mentor',$participant->mentor) == 0) selected @endif>Nee</option>
        <option value="1" @if(old('mentor',$participant->mentor) == 1) selected @endif>Ja</option>
    </select>
</div>


