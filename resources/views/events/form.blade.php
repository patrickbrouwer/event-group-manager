@include('partials.formError')

{{ csrf_field() }}
<div class="form-group">
    <label>Naam </label>
    <input type="text" class="form-control" name="name" value="{{ old('name',$event->name) }}">
</div>

<div class="form-group">
    <label>Omschrijving</label>
    <input type="text" class="form-control" name="description" value="{{ old('description',$event->description) }}">
</div>

<div class="form-group">
    <label>Evenement start</label>
    <input type="date" class="form-control" name="start_date" value="{{ old('start_date',$event->start_date) }}">
</div>


<div class="form-group">
    <label>Evenement start</label>
    <input type="date" class="form-control" name="end_date" value="{{ old('end_date',$event->end_date) }}">
</div>

<div class="form-group">
    <label>registratie start</label>
    <input type="date" class="form-control" name="register_start" value="{{ old('register_start',$event->register_start) }}">
</div>

<div class="form-group">
    <label>registratie eind</label>
    <input type="date" class="form-control" name="register_end" value="{{ old('register_end',$event->register_end) }}">
</div>


