@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Evenementen bewerken</h1>
        <form action="{{route('event.update',$event->id)}}" method="post">
            {!! method_field('patch') !!}
            @include('events.form')

            <button type="submit" class="btn btn-success">Opslaan</button>
        </form>
    </div>
@endsection