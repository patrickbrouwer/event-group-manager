@include('partials.formError')

{{ csrf_field() }}

<div class="form-group">
    <label for="sexSelector">Geslacht</label>
    <select class="form-control" name="sex" id="sexSelector" required>
        <option value="M">Man</option>
        <option value="F">Vrouw</option>
    </select>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label>Voornaam </label>
            <input type="text" class="form-control" name="first_name" required
                   value="{{ old('first_name',$participant->first_name) }}">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Achternaam</label>
            <input type="text" class="form-control" name="last_name" required
                   value="{{ old('last_name',$participant->last_name) }}">
        </div>
    </div>
</div>

<div class="form-group">
    <label>Geboortedatum</label>
    <input type="date" class="form-control" name="birthdate" required
           value="{{ old('birthdate',$participant->birthdate) }}">
</div>

<div class="form-group">
    <label>Email</label>
    <input type="email" class="form-control" name="email" required value="{{ old('email',$participant->email) }}">
    <small class="form-text text-muted">Dit e-mail adres wordt gebruikt voor communicatie voor evenement informatie
    </small>
</div>

<div class="form-group">
    <label>adres + huisnummer</label>
    <input type="text" class="form-control" name="address" required value="{{ old('address',$participant->address) }}">
</div>

<div class="form-group">
    <label>Stad</label>
    <input type="text" class="form-control" name="city" required value="{{ old('city',$participant->city) }}">
</div>

<div class="form-group">
    <label>telefoon</label>
    <input type="tel" class="form-control" name="phone" required value="{{ old('phone',$participant->phone) }}">
</div>

<div class="form-group">
    <label>Telfoon calamiteit</label>
    <input type="tel" class="form-control" name="phone_emergency" required
           value="{{ old('phone_emergency',$participant->phone_emergency) }}">
</div>

<div class="form-group">
    <label for="departmentSelector">Afdeling</label>
    <select class="form-control" name="department" required id="departmentSelector">
        <option value="schoonspringen">Schoonspringen</option>
        <option value="Synchroonzwemmen">Synchroonzwemmen</option>
        <option value="Waterpolo">Waterpolo</option>
        <option value="Zwemmen">Zwemmen</option>
    </select>
</div>

<div class="form-group">
    <label for="i4" class="control-label">Shirtmaat</label>
    <select class="form-control" name="shirt_size" required="">
        <option value="140">140</option>
        <option value="152">152</option>
        <option value="164">164</option>
        <option value="XS">xs</option>
        <option value="S">S</option>
        <option value="M">M</option>
        <option value="L">L</option>
        <option value="XL">XL</option>
        <option value="XXL">XXL</option>
    </select>
</div>

<!-- allergie -->
<div class="form-group">
    <div class="radio">
        <label>
            <input type="radio" name="allergieOption" value="ja" required>
            Deelnemer heeft een allergie
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="allergieOption" value="nee" required>
            Deelnemer heeft <strong>geen</strong> allergie
        </label>
    </div>
</div>
<div class="form-group" id="allergieForm">
    <label>Allergie&euml;n</label>
    <textarea class="form-control" name="allergies">{{ old('allergies',$participant->allergies) }}</textarea>
</div>

<!-- dieet -->
<div class="form-group">
    <div class="radio">
        <label>
            <input type="radio" name="dieetOption" value="ja" required>
            Deelnemer heeft dieetwensen
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="dieetOption" value="nee" required>
            Deelnemer heeft <strong>geen</strong> dieetwensen
        </label>
    </div>
</div>
<div class="form-group" id="dieetForm">
    <label>Dieet</label>
    <textarea class="form-control" name="diet">{{ old('diet',$participant->diet) }}</textarea>
</div>

<!-- medisch -->
<div class="form-group">
    <div class="radio">
        <label>
            <input type="radio" name="medischOption" value="ja" required>
            Deelnemer heeft een medische aandoeling of gebruikt medicijnen
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="medischOption" value="nee" required>
            Deelnemer heeft <strong>geen</strong> medische aandoeling of gebruikt medicijnen
        </label>
    </div>
</div>

<div class="form-group" id="medischForm">
    <label>Medische bijzonderheden</label>
    <textarea class="form-control" name="medical">{{ old('medical',$participant->medical) }}</textarea>
</div>

<div class="form-group">
    <label>Opmerkingen</label>
    <textarea class="form-control" name="comments">{{ old('comments',$participant->comments) }}</textarea>
    <small class="form-text text-muted">vermeld ook als deelnemer slaapwandeld of graag taart uitdeelt aan de
        begeleiding
    </small>
</div>


<h2>Calamiteiten informatie</h2>

<div class="form-group">
    <label>ID kaart of pasport documentnummer</label>
    <input type="text" class="form-control" name="document_nr" required
           value="{{ old('document_nr',$participant->document_nr) }}" pattern="([A-Z]){2}[A-Z0-9]{6}[0-9]{1}">
    <small class="form-text text-muted">Dit is niet het BSN nummer. voor meer <a
                href="https://www.rvig.nl/reisdocumenten/vraag-en-antwoord/hoe-is-het-paspoortnummer-of-identiteitskaartnummer-opgebouwd">informatie
            klik hier</a></small>
</div>


<div class="form-group">
    <label>Verzekeraar</label>
    <input type="text" class="form-control" name="insurer" required value="{{ old('insurer',$participant->insurer) }}">
</div>

<div class="form-group">
    <label>Polis nummer</label>
    <input type="text" class="form-control" name="insurer_number" required
           value="{{ old('insurer_number',$participant->insurer_number) }}">
</div>