@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Aanmelden voor {{$event->name}}</h1>
        <p>
            Datum:
            <time datetime="{{$event->start_date}}">{{$event->start_date}}</time>
            -
            <time datetime="{{$event->end_date}}">{{$event->end_date}}</time>
        </p>

        <p>
            {{$event->description}}
        </p>

        <hr/>
        <form action="{{route('form.update',[$event->id,$participant->id])}}" method="post">
            {!! method_field('patch') !!}
            @include('events.participant-form.form')

            <button type="submit" class="btn btn-success">Opslaan</button>
        </form>
    </div>
@endsection