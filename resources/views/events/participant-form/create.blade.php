@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Aanmelden voor {{$event->name}}</h1>
        <p>
            Datum:
            <time datetime="{{$event->start_date}}">{{$event->start_date}}</time>
            -
            <time datetime="{{$event->end_date}}">{{$event->end_date}}</time>
        </p>

        <p>
            {{$event->description}}
        </p>

        <hr/>

        <form action="{{route('form.store',$event->id)}}" method="post">

            @include('events.participant-form.form',['participant' =>  new \App\Participant()])

            <button type="submit" class="btn btn-success">Opslaan</button>
        </form>
    </div>

@endsection

@section('script')
    <script>
        $('#allergieForm').hide();
        $('#medischForm').hide();
        $('#dieetForm').hide();

        $('input[name=allergieOption]').change(function(){
            console.log($(this).val())
            if($(this).val() == "ja") {
                $('#allergieForm').show(250).find('textarea').prop('required', true)
            } else {
                $('#allergieForm').hide(250).find('textarea').prop('required', false)
            }
        });

        $('input[name=dieetOption]').change(function(){
            console.log($(this).val())
            if($(this).val() == "ja") {
                $('#dieetForm').show(250).find('textarea').prop('required', true)
            } else {
                $('#dieetForm').hide(250).find('textarea').prop('required', false)
            }
        });

        $('input[name=medischOption]').change(function(){
            console.log($(this).val())
            if($(this).val() == "ja") {
                $('#medischForm').show(250).find('textarea').prop('required', true)
            } else {
                $('#medischForm').hide(250).find('textarea').prop('required', false)
            }
        });
    </script>
@endsection
