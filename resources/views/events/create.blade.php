@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Evenementen aanmaken</h1>

        <form action="{{route('event.store')}}" method="post">

            @include('events.form',['event' =>  new \App\Events()])

            <button type="submit" class="btn btn-success">Opslaan</button>
        </form>
    </div>
@endsection