@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1>Evenement {{$event->name}}</h1>

        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mr-2" role="group" aria-label="First group">
                <a href="{{route('event.edit',$event->id)}}" class="btn btn-info">Bewerken</a>
            </div>
            <div class="btn-group mr-2" role="group" aria-label="Second group">
                <a href="#" class="btn btn-danger">Verwijderen</a>
            </div>
        </div>
        <p>
            Datum:
            <time datetime="{{$event->start_date}}">{{$event->start_date}}</time>
            -
            <time datetime="{{$event->end_date}}">{{$event->end_date}}</time>
        </p>

        <p>
            {{$event->description}}
        </p>

        <strong>Totaal deelnemers: {{$particiapants->count()}}</strong>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Naam</th>
                <th>Geboortedatum</th>
                <th>Mentor</th>
                <th>Betaald</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                @foreach($particiapants as $participant)
                    <tr>
                        <td>{{$participant->id}}</td>
                        <td>{{$participant->first_name}} {{$participant->last_name}}</td>
                        <td>{{date('d-m-Y', strtotime($participant->birthdate)) }}</td>
                        <td>@if($participant->mentor == 1) <span class="text-success">Ja</span> @else Nee @endif </td>
                        <td>@if($participant->payed == 1) <span class="text-success">Ja</span> @else <span class="text-danger">Nee</span> @endif </td>
                        <td><a href="{{route('participant.show',$participant->id)}}" class="btn btn-sm btn-info">Detail</a> </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@endsection