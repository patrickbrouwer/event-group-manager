@extends('layouts.app')

@section('content')
    <div class="container-fluid">
    <h1>Evenementen</h1>

    <p>
        <a href="{{route('event.create')}}" rel="noopener" class="btn btn-success">Maak event aan</a>
    </p>

    <table class="table ">
        <thead>
            <tr>
                <th>#</th>
                <th>naam</th>
                <th>start Datum</th>
                <th>end Datum</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
        @foreach($events as $event)
            <tr>
                <td><a href="{{route('event.show',$event->id)}}">{{$event->id}}</a></td>
                <td>{{$event->name}}</td>
                <td>{{$event->startDate}}</td>
                <td>{{$event->endDate}}</td>
                <td><a href="{{route('event.show',$event->id)}}" class="btn btn-info btn-sm">Detail</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@endsection