                @if(Session::has('SUCCESS'))
                    <div class="alert alert-success alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        {!! Session::get('SUCCESS')  !!}
                    </div>
                @endif

                @if(Session::has('ERROR'))
                    <div class="alert alert-danger alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        {!! Session::get('ERROR') !!}
                    </div>
                @endif