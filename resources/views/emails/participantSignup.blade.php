@component('mail::message')
# Bedankt voor je inschrijving voor {{$event->name}}

hierbij een kopie van de ingevulde gegevens:

Geslacht: @if($participant->sex == 'M') Man @else Vrouw @endif<br/>
Naam: {{$participant->first_name}} {{$participant->last_name}}<br/>
Geboortedatum: {{date('d-m-Y',strtotime($participant->birthdate))}}<br/>
adres: {{$participant->address}}, {{$participant->city}}<br/>
telfoon: {{$participant->phone}}<br/>
telfoon calamiteiten: {{$participant->phone_emergency}}<br/>
Afdeling: {{$participant->department}}<br/>
Shirt maat: {{$participant->shirt_size}}<br/>
Allergieen: {{$participant->allergies}}<br/>
Dieet: {{$participant->diet}}<br/>
Medische bijzonderheden: {{$participant->medical}}<br/>
Overig: {{$participant->comments}}<br/>
Document nr: {{$participant->document_nr}}<br/>
Verzekeraar: {{$participant->insurer}}<br/>
Posisnummer: {{$participant->insurer_number}}<br/>

Indien er iets niet klopt kunt u dit zelf aanpassen
@component('mail::button', ['url' => config('app.url').'/event/'.$event->id.'/form/'.$participant->id.'/edit'])
Pas je inschrijving aan
@endcomponent

Met vriendelijke groet,<br>
{{ config('app.name') }} team
@endcomponent
