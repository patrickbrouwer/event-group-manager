<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'first_name', 'last_name', 'sex', 'mentor', 'birthdate', 'email', 'address', 'city', 'phone',
        'phone_emergency', 'department', 'shirt_size', 'allergies', 'diet', 'medical', 'comments', 'document_nr',
        'insurer', 'insurer_number'
    ];

    public function setBirthdateAttribute($value) {
        $this->attributes['birthdate'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
    }
}