<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events;
use App\Participant;

class ParticipantSignup extends Mailable
{
    use Queueable, SerializesModels;


    public $event;
    public $participant;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Events $events, Participant $participant)
    {
        $this->event = $events;
        $this->participant = $participant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.participantSignup')->subject('confirmatie deelname '.$this->event->name);
    }
}
