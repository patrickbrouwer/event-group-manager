<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'start_date', 'end_date', 'description','register_start','register_end'
    ];

    public function setEndDateAttribute($value) {
        $this->attributes['end_date'] = \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function setStartDateAttribute($value) {
        $this->attributes['start_date'] = \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function setRegisterStartAttribute($value) {
        $this->attributes['register_start'] = \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function setRegisterEndAttribute($value) {
        $this->attributes['register_end'] = \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function getStartDateAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getEndDateAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getRegisterStartAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getRegisterEndAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }
}
