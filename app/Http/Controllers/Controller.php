<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Show 403 page
     * @return View
     */
    public static function show403()
    {
        return abort(403, "geen toegang");
    }

    /**
     * Show 404 page
     * @return View
     */
    public static function show404()
    {
        return abort(404);
    }
}
