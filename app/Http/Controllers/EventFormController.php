<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventFormRequest;
use App\Mail\ParticipantSignup;
use App\Participant;
use Illuminate\Http\Request;
use App\Events;
use Illuminate\Support\Facades\Mail;

class EventFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Events $events)
    {
        if ($events->register_start != null && $events->register_end != null ) {
            if (strtotime($events->register_start) >= time() && time() > strtotime($events->register_end)) {

                return abort(403, 'geen toegang');
            }
        }

        return view('events.participant-form.create', ['event' => $events]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Events $events, EventFormRequest $request)
    {
        $participant = new Participant();
        $participant->event_id = $events->id;
        $participant->fill($request->all());
        $participant->save();


        Mail::to($participant)->send(new ParticipantSignup($events, $participant));

        $request->session()->flash('SUCCESS', 'Succesvol aangemeld');
        return view('events.participant-form.complete', ['event' => $events]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Events $events, $id)
    {
        $participant = Participant::find($id);

        if ($participant->event_id != $events->id) {
            abort(404);
        }

        return view('events.participant-form.edit', ['event' => $events, 'participant' => $participant]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Events $events, EventFormRequest $request, $id)
    {
        $participant = Participant::find($id);
        $participant->fill($request->all());
        $participant->save();

        $request->session()->flash('SUCCESS', 'Succesvol aangepast');
        return view('events.participant-form.complete', ['event' => $events]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
