<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'sex' => 'required|in:F,M',
            'mentor',
            'birthdate' => 'required|date',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required|min:10',
            'phone_emergency' => 'required|min:10',
            'department' => 'required',
            'shirt_size' => 'required',
            'allergies',
            'diet',
            'medical',
            'comments',
            'document_nr'  => 'required|regex:/^([A-Z]{2}[A-Z0-9]{6}[0-9]{1})/',
            'insurer'  => 'required',
            'insurer_number' => 'required'
        ];
    }
}
